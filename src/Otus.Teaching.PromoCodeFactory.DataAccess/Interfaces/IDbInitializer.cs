﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Interfaces
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
