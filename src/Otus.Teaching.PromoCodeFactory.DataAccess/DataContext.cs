﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<PromoCode> Promocodes { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<PromoCode>()
                .HasOne(item => item.Customer)
                .WithMany(item => item.Promocodes)
                .HasForeignKey(item => item.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<PromoCode>()
                .HasOne(item => item.Preference)
                .WithMany(item => item.Promocodes)
                .HasForeignKey(item => item.PreferenceId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<CustomerPreference>()
                .HasKey(item => new { item.CustomerId, item.PreferenceId });

            modelBuilder
                .Entity<CustomerPreference>()
                .HasOne(item => item.Customer)
                .WithMany(item => item.CustomerPreferences)
                .HasForeignKey(item => item.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<CustomerPreference>()
                .HasOne(item => item.Preference)
                .WithMany(item => item.CustomerPreferences)
                .HasForeignKey(item => item.PreferenceId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
