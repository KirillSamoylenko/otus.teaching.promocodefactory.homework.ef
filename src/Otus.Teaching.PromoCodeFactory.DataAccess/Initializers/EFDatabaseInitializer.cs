﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Initializers
{
    public class EFDatabaseInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EFDatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.Roles.AddRange(FakeDataFactory.Roles);
            _dataContext.SaveChanges();

            _dataContext.Employees.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();

            _dataContext.Customers.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);
            _dataContext.SaveChanges();
        }
    }
}
