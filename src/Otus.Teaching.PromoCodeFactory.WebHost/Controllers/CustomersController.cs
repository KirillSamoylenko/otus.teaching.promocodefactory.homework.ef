﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomersController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить информацию о всех клиентах
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить информацию о клиенте по его идентификатору
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };

            if (customer.Promocodes != null)
            {
                response.PromoCodes = new List<PromoCodeShortResponse>();
                response.PromoCodes = customer
                    .Promocodes
                    .Select(item => new PromoCodeShortResponse
                    {
                        BeginDate = item.BeginDate.ToString(),
                        Code = item.Code,
                        EndDate = item.EndDate.ToString(),
                        Id = item.Id,
                        PartnerName = item.PartnerName,
                        ServiceInfo = item.ServiceInfo
                    })
                    .ToList();
            }

            if (customer.CustomerPreferences != null)
            {
                response.Preferences = new List<PreferenceResponse>();
                response.Preferences = customer
                    .CustomerPreferences
                    .Select(item => new PreferenceResponse
                    {
                        Id = item.Preference.Id,
                        Name = item.Preference.Name
                    })
                    .ToList();
            }

            return response;
        }

        /// <summary>
        /// Создать информацию о новом клиенте
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = new List<CustomerPreference>();

            foreach (var pref in request.PreferenceIds)
            {
                preferences.Add(new CustomerPreference()
                {
                    PreferenceId = pref
                });
            }

            await _customerRepository.AddAsync(new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = preferences
            });

            return Ok();
        }

        /// <summary>
        /// Редактировать информацию о клиенте
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var preferences = new List<CustomerPreference>();
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            CustomerMapper.MapFromModel(request, customer);

            foreach (var prefId in request.PreferenceIds)
            {
                preferences.Add(new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    PreferenceId = prefId
                });
            }
            customer.CustomerPreferences.Clear();
            customer.CustomerPreferences = preferences;
            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить информацию о клиенте
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}