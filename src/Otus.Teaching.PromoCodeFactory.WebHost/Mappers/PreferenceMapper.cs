using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PreferenceMapper
    {
        public static Preference MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Preference preference = null)
        {
            if (preference == null)
            {
                preference = new Preference
                {
                    Id = Guid.NewGuid()
                };
            }

            preference.Name = model.Name;

            return preference;
        }
    }
}

