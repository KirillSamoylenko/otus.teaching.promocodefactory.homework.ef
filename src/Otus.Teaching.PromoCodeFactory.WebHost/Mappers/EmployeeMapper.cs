using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class EmployeeMapper
    {
        public static Employee MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    Id = Guid.NewGuid()
                };
            }

            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.Email = model.Email;

            return employee;
        }
    }
}

